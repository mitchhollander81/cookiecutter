class Singleton(type):
    """Singleton dictionary of all cookiecutter options"""
    
    # mangled reference to the instance.
    _instance = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instance:
            cls._instance[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instance[cls]
            

class Options(metaclass=Singleton):
    def __init__(self):
        from sys import platform
        self.platform = platform
        if "linux" in platform:
            pass
        elif "darwin" == platform:
            pass
        elif "win32" == platform:
            pass
    

opts = Options()