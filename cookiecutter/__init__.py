"""
Tools for creating, testing, & deploying Bifrost kernels to production
"""
__version__ = "0.1"
__author__ = "Mitch Hollander"
__copyright__ = "Copyright (c) Mitch Hollander 2023"
__all__ = [
    "Shell",
]

from .shell import Shell
from .options import opts
