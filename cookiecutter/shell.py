import os
import shlex
import subprocess as sp

class Shell:
    """ module to run shell commands from python

    Args:
        cwd: string or path-like, to set working directory
        interactive_mode: if True, don't return stdout but pretty print output
    """

    def __init__(self, cwd: str = None, verbose = True):
        self.verbose: bool = verbose
        self.cwd: str = cwd

    def run_command(self, command: str, allow_fail: bool = False):
        """ Execute a terminal command and return output

        Args:
            command = 'text string of command'

        Returns: (unless in interactive mode)
            stdout
            stderror
            pid
        """
        # Get CWD
        if self.cwd is None:
            cwd = os.getcwd()
        else: 
            cwd = os.path.abspath(self.cwd)
        
        # Want to ensure quoted substrings are not split
        split_command = shlex.split(command)

        # Log command
        if self.verbose:
            print(f"[ {cwd} ] $", *[ f"\"{arg}\"" for arg in split_command ])
            stdin, stdout, stderr = None, None, None
            capture_output = False
        else: 
            capture_output = True

        # Execute
        try:
            status = sp.run(
                split_command,
                capture_output=capture_output,
                cwd=cwd,
            )
        # Log outcome
        except Exception as e:
            print_red("FAIL panic")
            if not allow_fail:
                raise e

        if status.returncode:
            if not allow_fail:
                raise RuntimeError(f"{command}\n FAIL exit {status.returncode}")
            elif self.verbose:
                print(f"FAIL exit {status.returncode}")
        elif self.verbose:
            print("SUCCESS")

        return status