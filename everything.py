#!/usr/bin/env python3
"""End-to-end automation to deploy a django app."""

import shutil
import cookiecutter import Shell

poetry = shutil.which("poetry")
if not poetry:
    print("Python poetry is not on your path! Would you like to install it?\n\n"
          "Y: Install with default paths\n")
term = Shell(verbose=False)
